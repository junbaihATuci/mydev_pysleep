# Welcome to pysleep, a module of MednickDB

This module is a work in progress. It can be used stand alone or within the mednickdb environment

pysleep is a module for calculating various measures, statistics and useful information about sleep records
It currently contains two submodules:

sleep_architecture: Calculates traditional sleep archetecture measures from a list of sleep stages such as *minutes in stage*, *sleep efficiency*

sleep_fragmentation: Calculates measures of fragmentation and dynamics such as *number of wake transitions*, *transition probabilities*, and *stage duration distributions*

To come:

Functions that operate on PSG data to calculate band power, find spindles, rem + other features.
